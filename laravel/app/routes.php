<?php
Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('perfil/{slug?}', array('as' => 'perfil', 'uses' => 'PerfilController@index'));
Route::get('projetos/{categoria?}', array('as' => 'projetos', 'uses' => 'ProjetosController@index'));
Route::get('projetos/{categoria?}/{projeto?}', array('as' => 'projetos.detalhes', 'uses' => 'ProjetosController@detalhes'));
Route::get('contato', array('as' => 'contato', 'uses' => 'ContatoController@index'));
Route::post('enviarContato', array('as' => 'contato.envio', 'uses' => 'ContatoController@enviar'));

Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));

// Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::get('lembrar') == '1') ? true : false;

	if(Auth::attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::logout();
	return Redirect::to('painel');
}));

Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{
	Route::any('multi-upload', 'Painel\AjaxController@upload');
    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::resource('banners', 'Painel\BannersController');
	Route::resource('contato', 'Painel\ContatoController');
	Route::resource('projetos', 'Painel\ProjetosController');
	Route::resource('projetoimagens', 'Painel\ProjetoImagensController');
	Route::resource('frases', 'Painel\FrasesController');
	Route::resource('contatosrecebidos', 'Painel\ContatosEnviadosController');
	Route::resource('equipe', 'Painel\EquipeController');
	Route::resource('perfil', 'Painel\PerfilController');
//NOVASROTASDOPAINEL//
});