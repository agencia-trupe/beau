<?php

use \Contato, \Frase, \ContatosEnviados, \Mail;

class ContatoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$frase = Frase::slug('contato');
		$this->layout->content = View::make('frontend.contato.index')->with(compact('frase'))->with('contato', \Contato::first());
	}

	public function enviar()
	{
		$data['nome'] = htmlspecialchars(Input::get('nome'));
		$data['email'] = htmlspecialchars(Input::get('email'));
		$data['mensagem'] = htmlspecialchars(Input::get('mensagem'));
		
		if($data['nome'] && $data['email'] && $data['mensagem']){

			$contato = new ContatosEnviados;
			$contato->nome = $data['nome'];
			$contato->email = $data['email'];
			$contato->mensagem = $data['mensagem'];
			$contato->save();
		
			Mail::send('emails.contato', $data, function($message) use ($data)
			{
			    $message->to('contato@beau.com.br', 'Beau Arquitetura')
			    		->subject('Contato via Site')
			    		->replyTo($data['email'], $data['nome']);
			});
		}		
	}
}
