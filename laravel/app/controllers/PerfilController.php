<?php

use \Perfil, \Frase;

class PerfilController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index($slug = 'a_arquiteta')
	{
		if($slug == 'equipe'){
			$texto = new Perfil;
			$texto->titulo = "EQUIPE";
			$texto->texto = "<p>EM BREVE</p>";
			$texto->imagem = "201429102014_equipe.jpg";
		}else{			
			$texto = Perfil::where('slug', '=', $slug)->first();
			if(!$texto) App::abort('404');
		}
			
		$frase = Frase::slug('perfil');
		$this->layout->content = View::make('frontend.perfil.index')->with('texto', $texto)
																	->with(compact('frase'))
																	->with('slugAtiva', $slug);
		
	}

}
