<?php

use \PortfolioCategorias, \PortfolioProjetos, \PortfolioImagens;

class ProjetosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index($categoria_slug = 'todos')
	{
		$frase = Frase::slug('projetos');
		$lista_categorias = PortfolioCategorias::all();

		if($categoria_slug != 'todos'){

			$categoria = PortfolioCategorias::slug($categoria_slug);

			if(!$categoria) App::abort('404');

			$projetos = $categoria->projetos;

		}else{
			$projetos = PortfolioProjetos::ordenado();
		}

		$this->layout->content = View::make('frontend.projetos.index')->with(compact('frase'))
																	  ->with(compact('categoria_slug'))
																	  ->with(compact('projetos'))
																	  ->with(compact('lista_categorias'));
	}

	public function detalhes($categoria_slug = 'todos', $projeto_slug = '')
	{
		$frase = Frase::slug('projetos');
		$lista_categorias = PortfolioCategorias::all();

		$projeto = PortfolioProjetos::slug($projeto_slug);

		if(!$projeto) App::abort('404');
		
		$proximo = PortfolioProjetos::where('portfolio_categorias_id', '=', $projeto->portfolio_categorias_id)
									  	->where('ordem', '>', $projeto->ordem)
									  	->orderBy('ordem', 'asc')
									  	->first();

		$anterior = PortfolioProjetos::where('portfolio_categorias_id', '=', $projeto->portfolio_categorias_id)
									 	->where('ordem', '<', $projeto->ordem)
									 	->orderBy('ordem', 'desc')
									 	->first();
		
		$this->layout->content = View::make('frontend.projetos.detalhes')->with(compact('frase'))
																		 ->with(compact('categoria_slug'))
																	  	 ->with(compact('projeto'))
																	  	 ->with(compact('lista_categorias'))
																	  	 ->with(compact('proximo'))
																	  	 ->with(compact('anterior'));
	}
}
