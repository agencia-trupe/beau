<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, Perfil;

class PerfilController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public $limiteInsercao = '2';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.perfil.index')->with('registros', Perfil::all())
																	->with('limiteInsercao', $this->limiteInsercao);		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return Redirect::route('painel.perfil.index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Redirect::route('painel.perfil.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.perfil.edit')->with('registro', Perfil::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Perfil::find($id);

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		$imagem = Thumb::make('imagem', 500, null, 'perfil/');
		if($imagem) $object->imagem = $imagem;

		$remover = Input::get('remover_imagem');
		if($remover == "1") $object->imagem = '';

		$imagem2 = Thumb::make('imagem2', 500, null, 'perfil/');
		if($imagem2) $object->imagem2 = $imagem2;

		$remover2 = Input::get('remover_imagem2');
		if($remover2 == "1") $object->imagem2 = '';

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			return Redirect::route('painel.perfil.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return Redirect::route('painel.perfil.index');
	}

}