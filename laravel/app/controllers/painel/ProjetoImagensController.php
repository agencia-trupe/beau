<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, ProjetoImagens;

class ProjetoImagensController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.projetoimagens.index')->with('projeto', \PortfolioProjetos::findOrFail(Input::get('projeto')));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return Redirect::route('painel.projetoimagens.index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Redirect::route('painel.projetoimagens.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return Redirect::route('painel.projetoimagens.index');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$projeto_id = Input::get('projeto_id');
		$projeto = \PortfolioProjetos::findOrFail($projeto_id);

		ProjetoImagens::where('portfolio_projetos_id', '=', $projeto_id)->delete();

		$imagens = Input::get('imagem_id');
		if($imagens && is_array($imagens)){
			foreach ($imagens as $key => $value) {
				$obj = new ProjetoImagens;
				$obj->portfolio_projetos_id = $projeto_id;
				$obj->imagem = $value;
				$obj->corte = Tools::verificaCorteImagem($value);
				$obj->ordem = $key;
				$obj->save();
			}
		}

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Projeto alterado com sucesso.');
		return Redirect::route('painel.projetos.index', array('categoria' => $projeto->portfolio_categorias_id));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return Redirect::route('painel.projetoimagens.index');
	}

}