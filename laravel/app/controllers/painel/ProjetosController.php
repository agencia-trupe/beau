<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, PortfolioProjetos;

class ProjetosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$projetos = PortfolioProjetos::where('portfolio_categorias_id', '=', Input::get('categoria'))->orderBy('ordem', 'ASC')->get();
		$this->layout->content = View::make('backend.projetos.index')->with('registros', $projetos)
																	 ->with('filtro', Input::get('categoria'))
																	 ->with('categorias', \PortfolioCategorias::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.projetos.form')->with('categorias', \PortfolioCategorias::all())
																	->with('filtro', Input::get('categoria'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new PortfolioProjetos;

		$object->portfolio_categorias_id = Input::get('portfolio_categorias_id');
		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		
		$capa = Thumb::make('capa', 230, 230, 'projetos/capas/');
		if($capa) $object->capa = $capa;

		$object->ordem = Tools::defineOrdemProjeto($object->portfolio_categorias_id);
		
		try {

			$object->save();

			$object->slug = Tools::defineSlug(Input::get('titulo'), $object->id, 'portfolio_projetos');
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Projeto criado com sucesso.');
			return Redirect::route('painel.projetos.index', array('categoria' => $object->portfolio_categorias_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Projeto!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.projetos.edit')->with('registro', PortfolioProjetos::find($id))
																	->with('categorias', \PortfolioCategorias::all());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = PortfolioProjetos::find($id);

		$object->portfolio_categorias_id = Input::get('portfolio_categorias_id');
		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');

		$capa = Thumb::make('capa', 230, 230, 'projetos/capas/');
		if($capa) $object->capa = $capa;

		$object->slug = Tools::defineSlug(Input::get('titulo'), $id, 'portfolio_projetos');
		
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Projeto alterado com sucesso.');
			return Redirect::route('painel.projetos.index', array('categoria' => $object->portfolio_categorias_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Projeto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = PortfolioProjetos::find($id);
		$redirectId = $object->portfolio_categorias_id;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Projeto removido com sucesso.');

		return Redirect::route('painel.projetos.index', array('categoria' => $redirectId));
	}

}