<?php

class PerfilArquitetaSeeder extends Seeder {

    public function run()
    {
    	DB::table('perfil')->delete();
    	
        $data = array(
            array(
            	'titulo' => 'Natashe Soares',
            	'slug' => 'a_arquiteta',
				'texto' => '<p>Texto do Perfil</p>'				
            ),
            array(
            	'titulo' => 'Laís Rostini',
            	'slug' => 'parcerias',
				'texto' => '<p>Texto do Perfil Parcerias</p>'				
            )
        );

        DB::table('perfil')->insert($data);
    }

}