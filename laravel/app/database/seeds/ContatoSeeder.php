<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
    	DB::table('contato')->delete();
    	
        $data = array(
            array(
				'email' => 'contato@beau.arq.br',
				'telefone' => '+55 62 8187 4201',
				'facebook' => 'www.facebook.com/beau',
				'instagram' => 'www.instagram.com/beau'
            )
        );

        DB::table('contato')->insert($data);
    }

}