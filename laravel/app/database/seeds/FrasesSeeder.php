<?php

class FrasesSeeder extends Seeder {

    public function run()
    {
    	DB::table('frases')->delete();
    	
        $data = array(
            array(
            	'titulo' => 'Projetos',
				'slug' => 'projetos',
				'frase' => 'Lorem ipsum dolor sit amet'
            ),
            array(
            	'titulo' => 'Perfil',
				'slug' => 'perfil',
				'frase' => 'Lorem ipsum dolor sit amet'
            ),
            array(
            	'titulo' => 'Contato',
				'slug' => 'contato',
				'frase' => 'Lorem ipsum dolor sit amet'
            ),
        );

        DB::table('frases')->insert($data);
    }

}