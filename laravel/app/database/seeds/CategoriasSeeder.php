<?php

class CategoriasSeeder extends Seeder {

    public function run()
    {
    	DB::table('portfolio_categorias')->delete();
    	
        $data = array(
            array(
				'titulo' => 'RESIDENCIAIS',
				'slug' => 'residenciais'
            ),
            array(
				'titulo' => 'COMERCIAIS',
				'slug' => 'comerciais'
            ),
            array(
				'titulo' => 'INTERIORES',
				'slug' => 'interiores'
            ),
            array(
				'titulo' => 'MOSTRAS',
				'slug' => 'mostras'
            )
        );

        DB::table('portfolio_categorias')->insert($data);
    }

}