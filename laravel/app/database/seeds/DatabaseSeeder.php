<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('CategoriasSeeder');
		$this->call('ContatoSeeder');

		$this->call('FrasesSeeder');
		$this->call('PerfilArquitetaSeeder');
		$this->call('UsersSeeder');
	}

}
