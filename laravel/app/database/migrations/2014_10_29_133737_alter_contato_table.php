<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContatoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contato', function(Blueprint $table)
		{
			$table->string('linkedin')->after('instagram');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contato', function(Blueprint $table)
		{
			$table->dropColumn('linkedin');
		});
	}

}
