<?php

class PortfolioProjetos extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'portfolio_projetos';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function categoria()
    {
    	return $this->belongsTo('PortfolioCategorias', 'portfolio_categorias_id');
    }

    public function imagens()
    {
    	return $this->hasMany('ProjetoImagens', 'portfolio_projetos_id')->orderBy('ordem', 'asc');
    }

    public function scopeSlug($query, $slug_projeto)
    {
    	return $query->where('slug', '=', $slug_projeto)->first();
    }

    public function scopeOrdenado($query)
    {
    	return $query->orderBy('ordem', 'asc')->get();
    }
}