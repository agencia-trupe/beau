<?php

class PortfolioCategorias extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'portfolio_categorias';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function scopeSlug($query, $slug)
    {
    	return $query->where('slug', '=', $slug)->first();
    }
    
    public function projetos()
    {
    	return $this->hasMany('PortfolioProjetos', 'portfolio_categorias_id')->orderBy('ordem', 'asc');
    }
}