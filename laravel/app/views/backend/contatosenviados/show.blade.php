@section('conteudo')

    <div class="container add">

      	<h2>
        	Visualizar Contato
        </h2>  

	
		<div class="pad">

	    	@if(Session::has('sucesso'))
	    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
	        @endif

	    	@if($errors->any())
	    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
	    	@endif	


			<label>Nome</label>
			<div class="well">
				{{$registro->nome}}
			</div>
			
			<label>Email</label>
			<div class="well">
				{{$registro->email}}
			</div>
			
			<label>Mensagem</label>
			<div class="well">
				{{nl2br($registro->mensagem)}}
			</div>

			<a href="{{URL::route('painel.contatosrecebidos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</div>
    </div>
    
@stop