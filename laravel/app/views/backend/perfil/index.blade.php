@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Perfil @if(sizeof(Perfil::all()) < $limiteInsercao) <a href='{{ URL::route('painel.perfil.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Texto</a> @endif
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
            	<th>Área do Site</th>
                <th>Título</th>
				<th>Texto</th>
				<th>Imagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
            	<td>
            		@if($registro->slug == 'a_arquiteta')
            			Perfil - A Arquiteta
            		@elseif($registro->slug == 'parcerias')
            			Perfil - Parcerias
            		@endif
            	</td>
                <td>{{ $registro->titulo }}</td>
				<td>{{ Str::words(strip_tags($registro->texto), 15) }}</td>
				<td><img src='assets/images/perfil/{{ $registro->imagem }}' style='max-width:150px'></td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.perfil.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>						
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop