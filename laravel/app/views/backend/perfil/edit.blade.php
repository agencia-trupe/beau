@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto
        </h2>  

		{{ Form::open( array('route' => array('painel.perfil.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control ckePerfil" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>
				
				<div class="well">
					<label>Imagem 1</label><hr>

					<div class="form-group">
						@if($registro->imagem)
							Imagem 1 Atual<br>
							<img src="assets/images/perfil/{{$registro->imagem}}"><hr>
							<label for="inputImagem">Trocar Imagem 1</label>
							<input type="file" class="form-control" id="inputImagem" name="imagem">
							<hr>
							<label><input type="checkbox" value="1" name="remover_imagem"> Excluir Imagem</label>
						@else
							<label for="inputImagem">Adicionar Imagem 1</label>
							<input type="file" class="form-control" id="inputImagem" name="imagem">
						@endif
					</div>
				</div>

				<div class="well">
					<label>Imagem 2</label><hr>

					<div class="form-group">
						@if($registro->imagem2)
							Imagem 2 Atual<br>
							<img src="assets/images/perfil/{{$registro->imagem2}}"><hr>
							<label for="inputImagem">Trocar Imagem 2</label>
							<input type="file" class="form-control" id="inputImagem" name="imagem2">
							<hr>
							<label><input type="checkbox" value="1" name="remover_imagem2"> Excluir Imagem</label>
						@else
							<label for="inputImagem">Adicionar Imagem 2</label>
							<input type="file" class="form-control" id="inputImagem" name="imagem2">
						@endif
					</div>
				</div>
					

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.perfil.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop