@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Contato 
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Email</th>
				<th>Telefone</th>
				<th>Mídias Sociais</th>
				<th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->email }}</td>
				<td>{{ $registro->telefone }}</td>
				<th>
					@if($registro->facebook)
						<a href="{{ $registro->facebook }}" class="btn btn-sm btn-default" title="Facebook" target="_blank">Facebook</a>
						<br>
					@endif
					@if($registro->instagram)
						<a href="{{ $registro->instagram }}" class="btn btn-sm btn-default" style="margin-top:5px;" title="Instagram" target="_blank">Instagram</a>
						<br>
					@endif
					@if($registro->linkedin)
						<a href="{{ $registro->linkedin }}" class="btn btn-sm btn-default" style="margin-top:5px;" title="Linked In" target="_blank">Linked In</a>
					@endif
				</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.contato.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop