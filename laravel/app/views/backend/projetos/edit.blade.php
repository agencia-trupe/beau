@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Projeto
        </h2>  

		{{ Form::open( array('route' => array('painel.projetos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputCategoria">Categoria</label>					 
					<select	name="portfolio_categorias_id" class="form-control" id="inputCategoria" required>
						<option value="">Selecione</option>
						@if(sizeof($categorias))
							@foreach($categorias as $cat)
								<option value="{{$cat->id}}" @if($cat->id == $registro->portfolio_categorias_id) selected @endif>{{$cat->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>

				<div class="form-group">
					@if($registro->capa)
						Capa Atual<br>
						<img src="assets/images/projetos/capas/{{$registro->capa}}" style="max-width:400px;"><br>
					@endif
					<label for="inputImagem">Trocar Imagem de Capa</label>
					<input type="file" class="form-control" id="inputImagem" name="capa">
				</div>
				
				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control projeto" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.projetos.index', array('categoria' => $registro->portfolio_categorias_id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop