@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Projetos <a href='{{ URL::route('painel.projetos.create', array('categoria' => $filtro)) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Projeto</a>
    </h2>

    @if(sizeof($categorias))
    	<div class="btn-group">
	    	@foreach($categorias as $cat)
				<a href="{{URL::route('painel.projetos.index', array('categoria' => $cat->id))}}" title="{{$cat->titulo}}" class="btn btn-sm btn-default @if($cat->id == $filtro) btn-warning @endif">{{$cat->titulo}}</a>
	    	@endforeach
    	</div>
    @endif

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='portfolio_projetos'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Título</th>
				<th>Texto</th>
				<th>Imagens</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
				<td>{{ $registro->titulo }}</td>
				<td>{{ Str::words(strip_tags($registro->texto), 15) }}</td>
				<td><a href="{{URL::route('painel.projetoimagens.index', array('projeto' => $registro->id)) }}" title="gerenciar imagens do projeto" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-picture"></span> gerenciar imagens</a></td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.projetos.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.projetos.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop