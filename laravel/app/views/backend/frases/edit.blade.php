@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Frase
        </h2>  

		{{ Form::open( array('route' => array('painel.frases.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<div class="form-group">
					<label for="inputTítulo">Título da Seção</label>
					<input type="text" class="form-control" disabled value="{{$registro->titulo}}">
				</div>

				<div class="form-group">
					<label for="inputFrase">Frase</label>
					<input type="text" class="form-control" id="inputFrase" name="frase" value="{{$registro->frase}}" required>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.frases.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop