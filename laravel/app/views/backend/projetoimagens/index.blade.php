@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Imagens do Projeto <strong>{{$projeto->titulo}}</strong>
    </h2>

    {{ Form::open( array('route' => array('painel.projetoimagens.update', $projeto->id), 'files' => true, 'method' => 'put') ) }}
		
		<input type="hidden" name="projeto_id" value="{{$projeto->id}}">

		<div class="well">
			<label>Imagens</label>
			
			<div id="multiUpload">
				<div id="icone">
					<span class="glyphicon glyphicon-open"></span>
					<span class="glyphicon glyphicon-refresh"></span>
				</div>
				<p>
					Escolha as imagens do Projeto. Você pode selecionar mais de um arquivo ao mesmo tempo. Você também pode arrastar e soltar arquivos nesta área para começar a enviar.<br>
					Se preferir também pode utilizar o botão abaixo para selecioná-las.
				</p>
				<input id="fileupload" type="file" name="files[]" data-url="painel/multi-upload" multiple>					
			</div>
			<div id="listaImagens">
				@if(sizeof($projeto->imagens))
					@foreach($projeto->imagens as $k => $v)
						<div class='projetoImagem'>
				        	<img src='assets/images/projetos/thumbs/{{$v->imagem}}'>
				        	<input type='hidden' name='imagem_id[]' value="{{$v->imagem}}">
				        	<a href='#' class='btn btn-sm btn-danger' title='remover a imagem'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover imagem</strong></a>
			        	</div>
					@endforeach
				@endif
			</div>
			<div class="panel panel-default">
				<div class="panel-body">
			    	Você pode clicar e arrastar as imagens para ordená-las.
			  	</div>
			</div>
		</div>
	
		<button type="submit" title="Alterar" class="btn btn-success">Salvar</button>

	<a href="{{URL::route('painel.projetos.index', array('categoria' => $projeto->portfolio_categorias_id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
    
</div>

@stop