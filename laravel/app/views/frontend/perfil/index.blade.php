@section('conteudo')
    <div class="section-header">
        <div class="centro">
            <h2>Perfil</h2>
            <p>{{$frase->frase}}</p>
            <div class="detalhe"></div>
        </div>
    </div>

    <section id="perfil">
        <div class="centro">
            <div class="container-imagens">
                @if($texto->imagem)
                	<img src="assets/images/perfil/{{$texto->imagem}}" alt="{{$texto->titulo}}">
                @endif
                @if($texto->imagem2)
                	<img src="assets/images/perfil/{{$texto->imagem2}}" alt="{{$texto->titulo}}">
                @endif
            </div>

            <nav>
                <a href="perfil" class="arquiteta @if($slugAtiva == 'a_arquiteta') active @endif">A Arquiteta</a>
                <a href="perfil/parcerias" class="parcerias @if($slugAtiva == 'parcerias') active @endif">Parcerias</a>
                <a href="perfil/equipe" class="equipe @if($slugAtiva == 'equipe') active @endif">Equipe</a>
            </nav>

            <div class="container-textos">
                <h3>{{$texto->titulo}}</h3>
                {{$texto->texto}}
            </div>
        </div>
    </section>
@stop
