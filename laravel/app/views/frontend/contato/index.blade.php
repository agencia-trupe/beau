@section('conteudo')
    <div class="section-header">
        <div class="centro">
            <h2>Contato</h2>
            <p>{{$frase->frase}}</p>
            <div class="detalhe"></div>
        </div>
    </div>

    <section id="contato">
        <div class="centro">
            <form id="contato-form" action="">
                <input name="nome" id="nome" type="text" placeholder="Nome" required>
                <input name="email" id="email" type="email" placeholder="E-mail" required>
                <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
                <input type="submit" value="Enviar">
                <div class="validate-msg">Mensagem enviada com sucesso!</div>
            </form>
            <div class="contato-dados">
                <p>{{$contato->telefone}}</p>
                <a href="mailto:{{$contato->email}}">{{$contato->email}}</a>
            </div>
        </div>
    </section>
@stop
