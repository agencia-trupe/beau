<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=1000px, initial-scale=1">
    
    <meta name="keywords" content="" />

	<title>beau · arquitetura & design</title>
	<meta name="description" content="beau · arquitetura & design">
	<meta property="og:title" content="beau · arquitetura & design"/>
	<meta property="og:description" content="beau · arquitetura & design"/>

    <meta property="og:site_name" content="Beau"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<link href='http://fonts.googleapis.com/css?family=Dosis:200,400,600' rel='stylesheet' type='text/css'>
	
	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/fancybox/source/jquery.fancybox',
		'css/main',
		'css/'.str_replace('-', '', Route::currentRouteName())
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"><\/script>')</script>	
	
	@if(App::environment()=='local')
		<?=Assets::JS(array('vendor/modernizr/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('vendor/modernizr/modernizr'))?>
	@endif

	<script>
		// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		// (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
		// Date();a=s.createElement(o),
		// m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		// ga('create', 'UA-', '');
		// ga('send', 'pageview');
	</script>
</head>
<body>
    
    @if(str_is('home', Route::currentRouteName()))
    
	    <div id="home-menu">
	        @include('frontend.templates.menu', array('contato' => \Contato::first()))
	    </div>

    	@yield('conteudo')

    @else

	    <header>
	        @include('frontend.templates.menu', array('contato' => \Contato::first()))
	    </header>

		@yield('conteudo')

	    <footer>
	        <div class="centro">
	            <p class="copyright">©2014 <span>Beau Arquitetura</span> · Todos os direitos reservados</p>
	            <p class="trupe"><a href="http://www.trupe.net" target="_blank">Criação de Sites:</a> <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a></p>
	            <div class="detalhe"></div>
	        </div>
	    </footer>

    @endif

	<?=Assets::JS(array(
		'vendor/jquery-cycle2/build/jquery.cycle2',
		'vendor/fancybox/source/jquery.fancybox',
		'js/main'
	))?>

</body>
</html>
