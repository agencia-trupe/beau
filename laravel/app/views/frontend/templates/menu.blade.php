<div class="centro">
    <h1>beau . arquitetura & design</h1>
    <nav class="menu">
        <a href="home" @if(str_is('home', Route::currentRouteName())) class="active" @endif>Home</a>
        <a href="perfil" @if(str_is('perfil*', Route::currentRouteName())) class="active" @endif>Perfil</a>
        <a href="projetos" @if(str_is('projetos*', Route::currentRouteName())) class="active" @endif>Projetos</a>
        <a href="contato" @if(str_is('contato', Route::currentRouteName())) class="active" @endif>Contato</a>
    </nav>
    <ul class="menu-social">
    	@if($contato && $contato->facebook)
        	<li><a href="{{$contato->facebook}}" target="_blank" class="facebook">facebook</a></li>
        @endif
        @if($contato && $contato->instagram)
        	<li><a href="{{$contato->instagram}}" target="_blank" class="instagram">instagram</a></li>
        @endif
        @if($contato && $contato->linkedin)
        	<li><a href="{{$contato->linkedin}}" target="_blank" class="linkedin">linkedin</a></li>
        @endif
    </ul>
</div>