@section('conteudo')

    <div id="home-control">
        <div class="control">
            <a href="#" id="control-prev">anterior</a>
            <a href="#" id="control-next">próximo</a>
        </div>
        <div class="descricao">
            <p id="slide-projeto">@if(sizeof($banners) && isset($banners[0])) {{$banners[0]->titulo}} @endif</p>
            <p id="slide-categoria">@if(sizeof($banners) && isset($banners[0])) {{$banners[0]->categoria}} @endif</p>
        </div>
    </div>

    <div id="home-slideshow">
    	@if(sizeof($banners))
    		@foreach($banners as $banner)
    			<div class='slide' style="background-image:url('assets/images/banners/{{$banner->imagem}}')"" data-projeto="{{$banner->titulo}}" data-categoria="{{$banner->categoria}}"></div>        		
    		@endforeach
    	@endif
    </div>

@stop