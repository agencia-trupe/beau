@section('conteudo')

    <div class="section-header">
        <div class="centro">
            <h2>Projetos</h2>
            <p>{{$frase->frase}}</p>
            <div class="detalhe"></div>
        </div>
    </div>

    <section id="projetos">
        <div class="centro">
            <nav id="projetos-categorias">
                <a href="projetos" @if($categoria_slug == 'todos') class="active" @endif>Mostrar Todos</a>
            	@if(sizeof($lista_categorias))
            		@foreach($lista_categorias as $c)
            			<a href="projetos/{{$c->slug}}" @if($categoria_slug == $c->slug) class="active" @endif>{{ucfirst(strtolower($c->titulo))}}</a>	
            		@endforeach
            	@endif                
            </nav>
            <div class="projetos-thumbs-wrapper">
            	@if(sizeof($projetos))
            		@foreach($projetos as $projeto)
		            	<a href="projetos/{{$projeto->categoria->slug}}/{{$projeto->slug}}" class="projeto-thumb">
		                    <img src="assets/images/projetos/capas/{{$projeto->capa}}">
		                    <div>
		                        <p>
		                            {{$projeto->titulo}}<br>
		                            <span>{{$projeto->categoria->singular}}</span>
		                        </p>
		                    </div>
		                </a>		
            		@endforeach
            	@endif                
            </div>
        </div>
    </section>
@stop
