@section('conteudo')

    <div class="section-header">
        <div class="centro">
            <h2>Projetos</h2>
            <p>{{$frase->frase}}</p>
            <div class="detalhe"></div>
        </div>
    </div>

    <section id="projeto-detalhe">
        <div class="centro">
            <aside class="projeto-aside">
                <div class="projeto-dados">
                	<h3>{{$projeto->titulo}}</h3>
                	<p class="categoria">{{$projeto->categoria->singular}}</p>
                    <div>
                    	{{$projeto->texto}}                        
                    </div>
                </div>
                <nav>
                	@if(sizeof($anterior))
                    	<a href="projetos/{{$anterior->categoria->slug}}/{{$anterior->slug}}" class="projeto-anterior">Anterior</a>
                	@endif
                	@if(sizeof($proximo))
                    	<a href="projetos/{{$proximo->categoria->slug}}/{{$proximo->slug}}" class="projeto-proximo">Próximo</a>
                	@endif
                    <a href="projetos" class="projeto-todos">Mostrar todos projetos</a>
                </nav>
            </aside>
            <div class="projeto-fotos">
            	@if(sizeof($projeto->imagens))
            		@foreach($projeto->imagens as $img)
						<img src="assets/images/projetos/redimensionadas/{{$img->imagem}}" @if($img->corte == 'vertical') class="vertical" @endif>						
            		@endforeach
            	@endif                
            </div>
        </div>
    </section>
@stop