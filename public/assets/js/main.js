$(document).ready(function() {

    // full screen imgs
    var $window            = $(window),
        $slideShowImgs     = $('#home-slideshow img'),
        slideShowImgsRatio = 2000 / 1000;

    function resizeSlideShow() {
        if (($window.width() / $window.height()) < slideShowImgsRatio) {
          $slideShowImgs.css({'height':'100%','width':'auto'});
        } else {
          $slideShowImgs.css({'width':'100%','height':'auto'});
        }
    }

    $window.resize(resizeSlideShow).trigger('resize');


    // slideshow
    $('#home-slideshow').cycle({
        fx: 'fade',
        speed: 'slow',
        slides: '> div',
        next: '#control-next',
        prev: '#control-prev'
    });

    $('#home-slideshow').on('cycle-before', function(e, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag ) {
	    var $slideAtual = $(incomingSlideEl),
            projeto     = $slideAtual.attr('data-projeto'),
            categoria   = $slideAtual.attr('data-categoria');
        $('#slide-projeto').text(projeto);
        $('#slide-categoria').text(categoria);
	});


    // fix projeto aside
    var projetoAside = $('.projeto-aside'),
        asidePos     = projetoAside.position();

    $(window).scroll(function() {
        var windowPos   = $(window).scrollTop(),
            documentPos = $(document).outerHeight() - projetoAside.outerHeight() - 230;

        if (windowPos >= 230 && documentPos > windowPos) {
            projetoAside.attr('style', '').addClass('stick');
        } else {
            if (windowPos >= documentPos) {
                projetoAside.removeClass('stick').css({'position':'absolute', 'bottom':'20px'});
            } else {
                projetoAside.removeClass('stick');
            }
        }
    });



    // envio formulário
    $('#contato-form').submit(function(event) {
        event.preventDefault();
        form = this;
        $.post('enviarContato', {
        	nome : $('#nome').val(),
        	email : $('#email').val(),
        	mensagem : $('#mensagem').val()
        }, function(resposta){
	        form.reset();
	        $('.validate-msg').hide().fadeIn('slow');
        });
    });

});