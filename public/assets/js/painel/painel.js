jQuery(function($){
    $.datepicker.regional['pt-BR'] = {
        closeText: 'Fechar',
        prevText: '&#x3c;Anterior',
        nextText: 'Pr&oacute;ximo&#x3e;',
        currentText: 'Hoje',
        monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
        'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
        'Jul','Ago','Set','Out','Nov','Dez'],
        dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
        dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
});

function iconeUploadStart(){
	$('#multiUpload #icone .glyphicon-open').css('opacity', 0);
	setTimeout( function(){
		$('#multiUpload #icone .glyphicon-refresh').css({'display' : 'block', 'opacity': 1});
		$('#multiUpload #icone .glyphicon-open').css('display', 'none');
	}, 350);
}

function iconeUploadStop(){
	$('#multiUpload #icone .glyphicon-refresh').css('opacity', 0);
	setTimeout( function(){
		$('#multiUpload #icone .glyphicon-open').css({'display' : 'block', 'opacity': 1});
		$('#multiUpload #icone .glyphicon-refresh').css('display', 'none');
	}, 350);
}

$('document').ready( function(){

  	$('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	bootbox.confirm('Deseja Excluir o Registro?', function(result){
	      	if(result)
	        	form.submit();
	      	else
	        	$(this).modal('hide');
    	});
  	});

    $('table.table-sortable tbody').sortable({
        update : function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post('ajax/gravaOrdem', { data : serial , tabela : tabela });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle : $('.btn-move')
    }).disableSelection();

    $('#listaImagens').sortable().disableSelection();

	$('.btn-move').click( function(e){e.preventDefault();});

	$('.datepicker').datepicker();

    if($('textarea.ckePerfil').length){
        $('textarea.ckePerfil').ckeditor({
        	contentsCss : '/assets/css/ckeditorPerfil.css'
        });

        CKEDITOR.stylesSet.add( 'meus_estilos_customizados', [
            { name: 'Parágrafo Normal',  element: 'p' },
            { name: 'Parágrafo Destaque',  element: 'p', attributes: { 'class': 'olho' } }
        ]);        
    }

    if($('textarea.projeto').length){
        $('textarea.projeto').ckeditor({
        	contentsCss : '/assets/css/ckeditorProjetos.css',
        	removeButtons : 'Styles'
        });
              
    }

    if($('#fileupload').length){

    	$('#fileupload').fileupload({
	        dataType: 'json',
	        start: function(e, data){
	        	iconeUploadStart();
			},
	        done: function (e, data) {
	        	var imagem = "<div class='projetoImagem'>";
	        	imagem += "<img src='"+data.result.files[0].thumb+"'>";
	        	imagem += "<input type='hidden' name='imagem_id[]' value='"+data.result.files[0].filename+"'>";
	        	imagem += "<a href='#' class='btn btn-sm btn-danger' title='remover a imagem'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover imagem</strong></a>";
	        	imagem += "</div>";
	            $('#listaImagens').append(imagem);
	            iconeUploadStop();
	        }
	    });

	    $(document).on('click', '.projetoImagem a', function(e){
	    	e.preventDefault();
	    	var parent = $(this).parent();
	    	parent.css('opacity', .35);
	    	setTimeout( function(){
	    		parent.remove();
	    	}, 350);
	    });

	}
});	
