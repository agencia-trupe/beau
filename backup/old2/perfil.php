<link href="css/beau.css.css" rel="stylesheet" type="text/css">
<table width="500" height="340" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="500" height="30" valign="top" class="menu_titulos" >A Arquiteta</td>
  </tr>
  <tr>
    <td width="500" height="2" align="center" valign="top"><img src="site.imagens/bara.2px.cinza.png" width="500" height="2" /></td>
  </tr>
  <tr>
    <td width="500" height="2" align="center" valign="top"><img src="site.imagens/bara.2px.transparente.png" width="500" height="2" /></td>
  </tr>
  <tr>
    <td width="500" height="306" valign="top" align="justify"><table width="220" height="210"border="0" align="left" cellpadding="0" cellspacing="0">
      <tr>
        <td><img src="site.imagens/foto.200x200.jpg" alt="" width="200" height="200" /></td>
      </tr>
    </table>
      <span class="menu_titulos">Nastashe Soares</span><br />
    <span class="text">Arquiteta e urbanista graduada pela Universidade de Brasília em 2011. Através do escritório BEAU Arquitetura e Design, o qual surgiu da necessidade de criar uma identidade para o seu trabalho, busca reproduzir e materializar os desejos e sonhos das pessoas em ambientes agradáveis e confortáveis.<br>Originalidade, funcionalidade, linguagem contemporânea, bases neutras, cores em detalhes e principalmente respeito ao cliente caracterizam o trabalho da profissional e contribuem para a formação do conceito do escritório. </span></td>
  </tr>
</table>

