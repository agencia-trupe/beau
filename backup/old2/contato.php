<script type="text/javascript">
    $(document).ready(function()
    {
    	$("#nome").focus();

        //Quando o formulário for submetido
        $("#signupForm").submit(function()
    	{
            //Exibe um texto na div #resposta assim que a requisição é iniciada
            $(this).ajaxStart(function()
    		{
                $("#resposta").html("<span class='menu_destaque'>Processando...</span>").show();

    		});
            //Opções a serem enviadas pela função
            var options =
    		{
                // destino: onde será exibida a resposta da página requisitada, no caso a div #resposta
    			target: "#resposta",
    			// aqui a página que será requisitada
                url: "contato_2.php",
    			// metodo de envio, post ou get
                type: "post",
                // caso a função tenha sucesso
                success: function(resposta)
    			{
    				// mostra a resposta na div resposta e exibe a div
                    $("#resposta").html("").show();
    				if(resposta=="ok")
    				{
    				    jQuery("form")[0].reset();
    					alert("Mensagem enviada com sucesso!");
    				}
    				else
    				{
    					alert(resposta);
    				}
    			}
    		}
            // aqui eu envio os dados com as opções
            $(this).ajaxSubmit(options);
            // isso é para que o formulário não envio os dados, pois os mesmos serão enviados por ajax
            return false;
    	});
    });
</script>
<table width="500" height="340" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="500" height="30" valign="top" class="menu_titulos" >Contato</td>
  </tr>
  <tr>
    <td width="500" height="2" align="center" valign="top"><img src="site.imagens/bara.2px.cinza.png" width="500" height="2" /></td>
  </tr>
  <tr>
    <td width="500" height="2" align="center" valign="top"><img src="site.imagens/bara.2px.transparente.png" width="500" height="2" /></td>
  </tr>
  <tr>
    <td width="500" height="306" valign="top" align="justify"><table width="500"height="306" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" valign="top">Essa é a foma direta de contato com o Beau Arquitetura.<br />
          Se preferir, você pode entrar em contato pelo telefone +55 62  81874201 </td>
      </tr>
      <tr><form name="signupForm" id="signupForm" method="post" action="" enctype="multipart/form-data">
        <td valign="top">Nome</td>
        <td valign="top"><input name="nome" type="text" class="text" id="nome2" size="37"  /></td>
      </tr>
      <tr>
        <td valign="top">E-mail</td>
        <td valign="top"><input name="email" type="text" class="text" id="email" size="37"  /></td>
      </tr>
      <tr>
        <td valign="top">Mensagem</td>
        <td valign="top"><textarea name="mensagem" cols="35"  rows="4" class="text" id="mensagem"></textarea></td>
      </tr>
      <tr>
        <td valign="top"></td>
        <td valign="top"><input name="Submit" type="submit" class="botao_padrao" value="Enviar" />
          <input name="Limpar" type="reset" class="botao_padrao" id="Limpar" value="Limpar" /></td>
      </tr>
      </form>
      <tr>
        <td width="100" valign="top"></td>
        <td width="400" valign="top"><div id="resposta"></div></td>
      </tr>
    </table></td>
  </tr>
</table>

