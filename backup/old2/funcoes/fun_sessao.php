<?php
	function fun_sessao_iniciar() {
	    //Define o limitador de cache para 'private'
        session_cache_limiter("private");
        $cache_limiter = session_cache_limiter();

        //Define o limite de tempo do cache em 20 minutos
        session_cache_expire(0);
        $cache_expire=session_cache_expire();

        //Inicializa a sessão
        session_start();
	}
?>