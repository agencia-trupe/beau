<?php
    //Troca caracteres (chr34),(chr39),(chr180),(chr96) por "'�`
	function fun_troca_caractere_exibicao($string) {
        $string=str_replace("(chr34)",chr(34),$string);
        $string=str_replace("(chr39)",chr(39),$string);
        $string=str_replace("(chr180)",chr(180),$string); // somente para o caso de j� existir algum texto com a constantee (chr180), pois ela n�o � mais usada por dar erro com o caractere "�"
        $string=str_replace("(chr96)",chr(96),$string);
        $string=htmlspecialchars($string);
        return $string;
	}

    //Troca caracteres "'�` por (chr34),(chr39),(chr180),(chr96)
	function fun_troca_caractere_banco($string)	{
        $string=str_replace(chr(34),"(chr34)",$string);
        $string=str_replace(chr(39),"(chr39)",$string);
        $string=str_replace(chr(96),"(chr96)",$string);		
        return $string;		
	}

?>
