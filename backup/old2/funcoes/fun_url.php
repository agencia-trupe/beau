<?php
	// indica se o site está dentro de uma pasta no servidor: muito usado em ambiente de desenvolvimento
	$sSub_root = ""; // $sSub_root = "site_sic";
	
	
	// Substitui o valor de um parâmetro de uma URL pelo valor indicado
	// Caso o parâmetro não exista, acrescenta o parâmetro no final da URL
	function fun_url_substituir($url, $chave_substituir, $valor_substituir){
		$asParametros = explode("&", $url);
		// Como cada posição de parametros possui um par chave=valor
		// vamos percorrer os parâmetros procurando qual deles é
		// a chave indicada para ter o valor trocado
		$bTrocou=false;
		$sRetorno = "";
		$bDeveRetornar = true;
		for ( $i=0; $i < count( $asParametros)-1; ) {
			$bDeveRetornar = true;
			
			$asParCV = explode("=", $asParametros[$i]);
			if($asParCV[0] == $chave_substituir) {
				$asParCV[1] = $valor_substituir;
				$bTrocou = true;
			}
			
			if(substr($url,0,1)=="&"){
				if($i==0) {
					$bDeveRetornar = false;
				}
			}
			
			if($bDeveRetornar==true) {
				$sRetorno = $sRetorno."&".$asParCV[0];
				if(isset($asParCV[1])) {
					$sRetorno = $sRetorno."=".$asParCV[1];
				}
			}
			$i++;
		}
		
		if(!$bTrocou) {
			$sRetorno= $sRetorno."&".$chave_substituir."=".$valor_substituir;
		}
		
		return $sRetorno; 
	}
	
	function fun_url_end_pagina($url)
	{
		$sRetorno = "";
		$asPartes = explode("?", $url);
		// A primeira posição contém o endereço sem os parâmetros
		// Agora temos que retirar o nome da página
		$iPosBarra = strrpos($asPartes[0], "/", 0);
		if($iPosBarra > 0) {
			$sRetorno = substr($asPartes[0],0,$iPosBarra);
		}
		return $sRetorno; 
	}
	
	function fun_url_root($sHTTP_HOST, $sPHP_SELF){
		Global $sSub_root;
		if (strlen($sSub_root) > 1) {
			$sSub_root = "/".$sSub_root;
		}
		$sRetorno = "http://".$sHTTP_HOST.$sSub_root;
		return $sRetorno;
	}

?>
