<?php
	function strPadL($sTexto, $sChar, $iLen) {
		return str_pad($sTexto,$iLen,$sChar,STR_PAD_LEFT); 
	}
	function strPadR($sTexto, $sChar, $iLen) {
		return str_pad($sTexto,$iLen,$sChar,STR_PAD_RIGHT); 
	}	
	function strPadC($sTexto, $sChar, $iLen) {
		return str_pad($sTexto,$iLen,$sChar,STR_PAD_BOTH); 
	}
	
	function strTrocaEspeciais($var) {
		$var = utf8_decode($var);
		$a = array(	"/[�����]/"=>"A",
					"/[�����]/"=>"a",
					"/[����]/"=>"E",
					"/[����]/"=>"e",
					"/[����]/"=>"I",
					"/[����]/"=>"i",
					"/[�����]/"=>"O",
					"/[�����]/"=>"o",
					"/[����]/"=>"U",
					"/[����]/"=>"u",
					"/�/"=>"c",
					"/�/"=> "C");
		// Tira o acento pela chave do array
		return preg_replace(array_keys($a), array_values($a), $var);
	}
	
	function strFormataNumero($sNum, $iCasasDec) {
		return number_format($sNum,$iCasasDec,',','.'); 
	}
	
	function strFormataNumeroDecimalPonto($sNum, $iCasasDec) {
		return number_format($sNum,$iCasasDec,'.',''); 
	}
	
	function ajustaValores($sNum) {
		$sNum=str_replace(".","",$sNum);
		$sNum=str_replace(",",".",$sNum);
		
		return $sNum;
	}
	
	define('_strIsUtf8_split',5000);
	function strIsUtf8($string) { // v1.01
		if (strlen($string) > _strIsUtf8_split) {
			// Based on: http://mobile-website.mobi/php-utf8-vs-iso-8859-1-59
			for ($i=0,$s=_strIsUtf8_split,$j=ceil(strlen($string)/_strIsUtf8_split);$i < $j;$i++,$s+=_strIsUtf8_split) {
				if (strIsUtf8(substr($string,$s,_strIsUtf8_split)))
					return true;
			}
			return false;
		} else {
			// From http://w3.org/International/questions/qa-forms-utf-8.html
			return preg_match('%^(?:
					[\x09\x0A\x0D\x20-\x7E]            # ASCII
				| [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
				|  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
				| [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
				|  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
				|  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
				| [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
				|  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
			)*$%xs', $string);
		}
	}
	
	function strToUtf8($in) {
		if (!strIsUtf8($in)) {
			if (is_array($in)) {
				foreach ($in as $key => $value) {
					$out[strToUtf8($key)] = strToUtf8($value);
				}
			} elseif(is_string($in)) {
				if(mb_detect_encoding($in) != "UTF-8")
					return utf8_encode($in);
				else
					return $in;
			} else {
				return $in;
			}
			return $out;
		} else {
			return $in;
		}
	} 
	
	function strUtf8ToISO8859($string,$to="iso-8859-9",$from="utf8") {
		if($to=="iso-8859-9" && $from=="utf8"){
			$str_array = array(
			   chr(196).chr(177) => chr(253),
			   chr(196).chr(176) => chr(221),
			   chr(195).chr(182) => chr(246),
			   chr(195).chr(150) => chr(214),
			   chr(195).chr(167) => chr(231),
			   chr(195).chr(135) => chr(199),
			   chr(197).chr(159) => chr(254),
			   chr(197).chr(158) => chr(222),
			   chr(196).chr(159) => chr(240),
			   chr(196).chr(158) => chr(208),
			   chr(195).chr(188) => chr(252),
			   chr(195).chr(156) => chr(220)
			   );
			return str_replace(array_keys($str_array), array_values($str_array), $string);  
		}   
		return $string;
	}
	
	function htmToUtf8 ($source) {
		if (strIsUtf8($source)) {
			return $source;
		}
		$decodedStr = '';
		$pos = 0;
		$len = strlen($source);

		while ($pos < $len) {
			$charAt = substr ($source, $pos, 1);
			if ($charAt == '?') {
				$char2 = substr($source, $pos, 2);
				$decodedStr .= htmlentities(utf8_decode($char2),ENT_QUOTES,'ISO-8859-1');
				$pos += 2;
			}
			elseif(ord($charAt) > 127) {
				$decodedStr .= "&#".ord($charAt).";";
				$pos++;
			}
			elseif($charAt == '%') {
				$pos++;
				$hex2 = substr($source, $pos, 2);
				$dechex = chr(hexdec($hex2));
				if($dechex == '?') {
					$pos += 2;
					if(substr($source, $pos, 1) == '%') {
						$pos++;
						$char2a = chr(hexdec(substr($source, $pos, 2)));
						$decodedStr .= htmlentities(utf8_decode($dechex . $char2a),ENT_QUOTES,'ISO-8859-1');
					}
					else {
						$decodedStr .= htmlentities(utf8_decode($dechex));
					}
				}
				else {
					$decodedStr .= $dechex;
				}
				$pos += 2;
			}
			else {
				$decodedStr .= $charAt;
				$pos++;
			}
		}

		return $decodedStr;
    }
	
	function LowerCase($string){
	
		$string = strtolower($string);
		$string = str_replace("�","�",$string);
		$string = str_replace("�","�",$string);
		$string = str_replace("�","�",$string);
		$string = str_replace("A","�",$string);
		$string = str_replace("�","�",$string);
		$string = str_replace("�","�",$string);
		$string = str_replace("I","�",$string);
		$string = str_replace("�","�",$string);
		$string = str_replace("�","�",$string);
		$string = str_replace("�","�",$string);
		$string = str_replace("�","�",$string);
		$string = str_replace("�","�",$string);
		$string = str_replace("U","�",$string);
		$string = str_replace("�","�",$string);
		return $string;
    }
   
	function strCapitalize($sStr) {
		$sRetorno = ucwords(LowerCase(strTrocaEspeciais($sStr)));
		$sRetorno = str_replace(" E "," e ",$sRetorno);
		$sRetorno = str_replace(" O "," o ",$sRetorno);
		$sRetorno = str_replace(" Em "," em ",$sRetorno);
		$sRetorno = str_replace(" De "," de ",$sRetorno);
		$sRetorno = str_replace(" Para "," para ",$sRetorno);
		return $sRetorno;
    }
?>