<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/main.css" type="text/css" media="screen" />
</head>
<body>

   	<form name="form" method="post" id="third" action="<?php echo $self;?>"  class="niceform">
		<h1>Portifólio</h1>
        <!-- Name -->
		<label for="name"><strong> RESIDENCIAIS:<br><br>

- Casa térrea de alto padrão com 400m² composta por área de lazer com piscina, varanda gourmet, churrasqueira e campo de futebol em grama natural, em terreno de 1.000m²;<br>

- Casa de veraneio médio padrão com 180m² sendo três pavimentos;<br>

- Casa térrea com 200m² de médio padrão.<br>
<br>


INSTALAÇÕES:<br><br>

- Execução de instalações de prevenção e combate a incêndio em galpão industrial com 16.000 m²;<br>

- Execução de instalações elétricas e hidrossanitárias em condomínio de sobrados com 650m².<br>
<br>



PROJETOS:<br><br>

Elaboração de projetos de:<br>
- Instalações elétricas;<br>
- Instalações hidrossanitárias;<br>
- Instalações de prevenção e combate a incêndio. </strong><br />
	  </label>
		
   	</form>


</body>
</html>