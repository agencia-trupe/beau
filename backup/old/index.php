<!--
Desenvolvido por André Luiz Vieira Fernandes
Goiânia / Goiás / Brasil
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BEAU Arquitetura & Design</title>
<link media="screen" rel="stylesheet" href="css/beau.css" />
<meta name="keywords" content="BEAU Arquitetura & Design"/> 
<link media="screen" rel="stylesheet" href="css/main.css" />
<!-- Início - Imagens do colorbox -->
<link media="screen" rel="stylesheet" href="colorbox/colorbox.css" />
<!-- Fim - Imagens do colorbox -->
<!-- Início - Imagens do background.slideshow -->
<link rel="stylesheet" type="text/css" href="background.slideshow/style.css" />
<script type="text/javascript" src="background.slideshow/js/jquery-1.2.6.min.js"></script>
<!-- Fim - Imagens do background.slideshow -->

<script src="colorbox/jquery.min.js"></script>
<script src="colorbox/jquery.colorbox.js"></script>
	<script>
		$(document).ready(function()
		{
			$(".projetos").colorbox({rel:'projetos', transition:"none"});
			$(".perfil").colorbox({iframe:true, width:"900px", height:"563px"});
			//$(".projetos").colorbox({iframe:true, width:"900px", height:"563px"});
			$(".contato").colorbox({iframe:true, width:"900px", height:"563px"});
			$(".callbacks").colorbox({
				onOpen:function(){ alert('onOpen: colorbox is about to open'); },
				onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
				onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
				onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
				onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
			});
			//Example of preserving a JavaScript event for inline calls.
			$("#click").click(function(){ 
				$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
				return false;
			});
		});
	</script>


<script type="text/javascript">

function slideSwitch() {
    var $active = $('#slideshow IMG.active');

    if ( $active.length == 0 ) $active = $('#slideshow IMG:last');

    // use this to pull the images in the order they appear in the markup
    var $next =  $active.next().length ? $active.next()
        : $('#slideshow IMG:first');

    // uncomment the 3 lines below to pull the images in random order
    
    // var $sibs  = $active.siblings();
    // var rndNum = Math.floor(Math.random() * $sibs.length );
    // var $next  = $( $sibs[ rndNum ] );


    $active.addClass('last-active');

    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 1000, function() {
            $active.removeClass('active last-active');
        });
}

$(function() {
    setInterval( "slideSwitch()", 5000 );
});

</script>	
	
<style type="text/css">
body 
{
   margin:0;
   padding:0;
   height:100%;
   min-width: 930px;
   min-height:100%;
}
body
{
	background: url('images/bg.jpg') no-repeat top center #ffffff; 
}
.clear 
{
    clear:both
}

#box
{
	background-color:#088b89; 
	height:563px; left:50%; 
	margin:-190px 0 0 -450px; 
	position:absolute; 
	top:40%; 
	width:900px
}

</style>
</head>
<body>
<!-- Início - Imagens do background.slideshow -->
<div id="slideshow">
    <img src="backgrouds/image1.jpg" alt="" class="active" />
    <img src="backgrouds/image2.jpg" alt="" />
    <img src="backgrouds/image3.jpg" alt="" />
    <img src="backgrouds/image4.jpg" alt="" />
    <img src="backgrouds/image5.jpg" alt="" />
	<img src="backgrouds/image6.jpg" alt="" />
    <img src="backgrouds/image7.jpg" alt="" />
    <img src="backgrouds/image8.jpg" alt="" />
    <img src="backgrouds/image9.jpg" alt="" />
</div>
<!-- Fim - Imagens do background.slideshow -->

<div id="box">
<table width="900px" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="900px" height="563px">
	<img src="images/final.jpg" width="900px" height="563px" border="0" usemap="#Map"/></td>
  </tr>
 </table>
<div>
<map name="Map" id="Map">
<area shape="rect" coords="198,103,250,125" class='perfil' href="perfil.php" />
<area shape="rect" coords="378,103,458,125" class='contato' href="contato.php" />
<area shape="rect" coords="273,103,353,125" class='projetos' href="projetos/projeto01.jpg" rel="projetos" title="Projeto Bla bla bla" />
</map>
<!-- Início - Imagens de projetos -->
<a class="projetos"  rel="projetos" href="projetos/projeto02.jpg" title="Projeto La la la"></a>
<a class="projetos"  rel="projetos" href="projetos/projeto03.jpg" title="Casa do Fulano"></a>
<a class="projetos"  rel="projetos" href="projetos/projeto04.jpg" title="Casa do Siclano"></a>
<a class="projetos"  rel="projetos" href="projetos/projeto05.jpg" title="Casa do Nova"></a>
<a class="projetos"  rel="projetos" href="projetos/projeto06.jpg" title="Projeto Li li li"></a>
<!-- Fim - Imagens de projetos -->
</body>
</html>