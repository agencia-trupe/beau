<?php
    function fun_remove_diretorio($file)
    {
        if(file_exists($file))
        {
            chmod($file,0755);
            if(is_dir($file))
            {
                $handle = opendir($file);
                while($filename = readdir($handle))
                {
                    if ($filename != "." && $filename != "..")
                    {
                        fun_remove_diretorio($file."/".$filename);
                    }
                }
                closedir($handle);
                rmdir($file);
            }
            else
            {
                unlink($file);
            }
        }
    }
?>
