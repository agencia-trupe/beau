<?php
	//Fun��o valida alpha numerico
	function alpha_numerico($str)
	{
		return ( ! preg_match("/^([-a-z0-9])+$/i", $str)) ? FALSE : TRUE;
	}

	//Fun��o valida e-mail
	function valida_email($str)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
	}

	//Fun��o valida imagem
	function valida_imagem($str)
	{
		$str=explode(".", $str);
		$extensao="$str[1]";
		if($extensao=="jpg" || $extensao=="JPG")
		{
		    return  TRUE;
		}
		else
		{
		    return  FALSE;
		}
	}
	
	//Fun��o valida imagem
	function valida_documento($str)
	{
		$str=explode(".", $str);
		$extensao="$str[1]";
		if($extensao=="doc" || $extensao=="docx" || $extensao=="txt" || $extensao=="pdf")
		{
		    return  TRUE;
		}
		else
		{
		    return  FALSE;
		}
	}
	
	//Fun��o valida imagem
	function valida_cep($str)
	{
		return ( ! preg_match("/^[0-9]{5}-[0-9]{3}$/", $str)) ? FALSE : TRUE;
	}
	
	//Fun��o valida imagem
	function valida_cpf($str)
	{
		return ( ! preg_match("/^([0-9]{3}\.){2}[0-9]{3}-[0-9]{2}$/", $str)) ? FALSE : TRUE;
	}
?>