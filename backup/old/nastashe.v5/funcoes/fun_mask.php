<?php
	// Colocar caracteres de máscara em um texto
	function mask_adicionar($sTexto, $sMask) {
		$sRetorno = "";
		$iTexto = 0;
		// Go off of the entered text and match it against the mask.
		for ( $i=0; $i < strlen($sMask); ) {
			if($iTexto < strlen($sTexto)) {
				switch (substr($sMask,$i,1)) {
					case "9":
					case "0":
					case "a":
					case "A":
					case "*":
						$sRetorno = $sRetorno.substr($sTexto,$iTexto,1);
						$iTexto = $iTexto + 1;
						break;
					default:
						$sRetorno = $sRetorno.substr($sMask,$i,1);
				}
			} else {
				switch (substr($sMask,$i,1)) {
					case "9":
					case "0":
					case "a":
					case "A":
					case "*":
						$sRetorno = $sRetorno." ";
						break;
					default:
						$sRetorno = $sRetorno.substr($sMask,$i,1);
				}
			}
			$i=$i+1;
		}
		return $sRetorno;
	}
?>