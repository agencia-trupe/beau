<?php

    function fun_data_banco($dData)
    {
		$retorno="NULL";
		if($dData!="") {
			$array=explode("/", $dData);
			$dia=$array[0];
			$mes=$array[1];
			$ano=$array[2];
			$traco="-";
			$retorno=$ano.$traco.$mes.$traco.$dia;
		}
        return $retorno;
    }

    function fun_data_exibicao_barra($data)
    {
		$retorno="";
		if(($data!="") && ($data!="NULL") && ($data!="00/00/0000") && ($data!="0000-00-00")) {
			$array=explode("-", $data);
			$dia=$array[2];
			$mes=$array[1];
			$ano=$array[0];
			$barra="/";
			$retorno=$dia.$barra.$mes.$barra.$ano;
		}
		return $retorno;
    }

    function fun_data_exibicao_traco($data)
    {
		$retorno="";
		if(($data!="") && ($data!="NULL") && ($data!="00/00/0000") && ($data!="0000-00-00")) {
			$array=explode("-", $data);
			$dia=$array[2];
			$mes=$array[1];
			$ano=$array[0];
			$barra="-";
			$retorno=$dia.$barra.$mes.$barra.$ano;
		}
		return $retorno;
    }

    function fun_data_input($data)
    {
		$retorno="";
		if(($data!="") && ($data!="NULL") && ($data!="00/00/0000") && ($data!="0000-00-00")) {
			$array=explode("-", $data);
			$dia=$array[2];
			$mes=$array[1];
			$ano=$array[0];
			$barra="/";
			$retorno=$dia.$barra.$mes.$barra.$ano;
		}
		return $retorno;
    }
	
	function calculaData($data,$operacao, $dias, $meses, $ano) {  
		//informe a data no formato dd/mm/yyyy   
		$data = explode("/", $data);   

		if ($operacao == '+') {
			$novaData = date("d/m/Y", mktime(0, 0, 0, $data[1] + $meses, $data[0] + $dias, $data[2] + $ano) );
		} else if ($operacao == '-') {
			$novaData = date("d/m/Y", mktime(0, 0, 0, $data[1] - $meses,$data[0] - $dias, $data[2] - $ano) );
		}   

		return $novaData;
	}
?>

