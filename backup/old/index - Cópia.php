<!--
Desenvolvido por André Luiz Vieira Fernandes
Goiânia / Goiás / Brasil
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BEAU Arquitetura & Design</title>
<meta name="keywords" content="BEAU Arquitetura & Design"/> 
<!-- Início - Imagens do colorbox -->
<link media="screen" rel="stylesheet" href="css/colorbox.css" />
<link media="screen" rel="stylesheet" href="css/main.css" />
<link media="screen" rel="stylesheet" href="css/beau.css" />
<!-- Fim - Imagens do colorbox -->
<!-- Início - Imagens do background.slideshow -->
<link rel="stylesheet" type="text/css" href="background.slideshow/style.css" />
<script type="text/javascript" src="background.slideshow/js/jquery-1.2.6.min.js"></script>
<!-- Fim - Imagens do background.slideshow -->

<script src="js/jquery.min.js"></script>
<script src="js/jquery.colorbox.js"></script>
	<script>
		$(document).ready(function()
		{
			$(".perfil").colorbox({iframe:true, width:"900px", height:"563px"});
			$(".projetos").colorbox({iframe:true, width:"900px", height:"563px"});
			$(".contato").colorbox({iframe:true, width:"900px", height:"563px"});
			$(".callbacks").colorbox({
				onOpen:function(){ alert('onOpen: colorbox is about to open'); },
				onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
				onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
				onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
				onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
			});
			//Example of preserving a JavaScript event for inline calls.
			$("#click").click(function(){ 
				$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
				return false;
			});
		});
	</script>


<script type="text/javascript">

function slideSwitch() {
    var $active = $('#slideshow IMG.active');

    if ( $active.length == 0 ) $active = $('#slideshow IMG:last');

    // use this to pull the images in the order they appear in the markup
    var $next =  $active.next().length ? $active.next()
        : $('#slideshow IMG:first');

    // uncomment the 3 lines below to pull the images in random order
    
    // var $sibs  = $active.siblings();
    // var rndNum = Math.floor(Math.random() * $sibs.length );
    // var $next  = $( $sibs[ rndNum ] );


    $active.addClass('last-active');

    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 1000, function() {
            $active.removeClass('active last-active');
        });
}

$(function() {
    setInterval( "slideSwitch()", 5000 );
});

</script>	
	
<style type="text/css">
body 
{
   margin:0;
   padding:0;
   height:100%;
   min-width: 930px;
   min-height:100%;
}
body
{
	background: url('images/bg.jpg') no-repeat top center #ffffff; 
}
.clear 
{
    clear:both
}

#box
{
	background-color:#088b89; 
	height:563px; left:50%; 
	margin:-190px 0 0 -450px; 
	position:absolute; 
	top:40%; 
	width:900px
}

</style>
</head>
<body>
<!-- Início - Imagens do background.slideshow -->
<div id="slideshow">
    <img src="backgrouds/image1.jpg" alt="Slideshow Image 1" class="active" />
    <img src="backgrouds/image2.jpg" alt="Slideshow Image 2" />
    <img src="backgrouds/image3.jpg" alt="Slideshow Image 3" />
    <img src="backgrouds/image1.jpg" alt="Slideshow Image 4" />
    <img src="backgrouds/image2.jpg" alt="Slideshow Image 5" />
</div>
<!-- Fim - Imagens do background.slideshow -->

<div id="box">
<table width="900px" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="900px" height="563px">
	<img src="images/final.jpg" width="900px" height="563px" border="0" usemap="#Map"/></td>
  </tr>
 </table>
<div>
<a class="fotos"  rel="fotos" href="fotos/2.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/3.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/4.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/5.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/6.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/7.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/8.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/9.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/10.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/11.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/12.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/13.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/13.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/14.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/15.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/16.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/17.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/18.jpg" title="5 Engenharia."></a>
<a class="fotos"  rel="fotos" href="fotos/19.jpg" title="5 Engenharia."></a>
<map name="Map" id="Map"><area shape="rect" coords="378,103,458,125" class='contato' href="contato.php" /><area shape="rect" coords="273,103,353,125" class='projetos' href="projetos.php" /><area shape="rect" coords="273,103,353,125" class='quemsomos' href="perfil.php" />
  <area shape="rect" coords="194,103,245,125" class='perfil' href="perfil.php" />
</map>
</body>
</html>