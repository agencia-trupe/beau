<link href="css/beau.css.css" rel="stylesheet" type="text/css">
<table width="500" height="375" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="500" height="30" valign="top" >A Arquiteta</td>
  </tr>
  <tr>
    <td width="500" height="5" align="center" valign="top"><img src="site.imagens/barra.5px.jpg" width="500" height="5" /></td>
  </tr>
  <tr>
    <td width="500" height="5" align="center" valign="top"><img src="site.imagens/bara.5px.transparente.png" width="500" height="5" /></td>
  </tr>
  <tr>
    <td width="500" height="335" valign="top"><table width="160" height="160"border="0" align="left" cellpadding="0" cellspacing="0">
      <tr>
        <td><img src="site.imagens/foto.jpg" alt="" width="150" height="150" /></td>
      </tr>
    </table>
      Nastashe Soares<br />
    Arquiteta e urbanista graduada pela Universidade de Brasília em 2011. Através do escritório BEAU Arquitetura e Design, o qual surgiu da necessidade de criar uma identidade para o seu trabalho, busca reproduzir e materializar os desejos e sonhos das pessoas em ambientes agradáveis e confortáveis. Originalidade, funcionalidade, linguagem contemporânea, bases neutras, cores em detalhes e principalmente respeito ao cliente caracterizam o trabalho da profissional e contribuem para a formação do conceito do escritório. </td>
  </tr>
</table>

