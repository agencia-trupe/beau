<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/main.css" type="text/css" media="screen" />
</head>
<body>
<form name="form" method="post" id="third" action="<?php echo $self;?>"  class="niceform">
	<h1>APRESENTAÇÃO</h1>
	<label for="name">
		<strong>
		

Apaixonada e interessada por arte.
O olhar que procura sempre uma plástica e também reflexão sobre o entorno marca o portfólio da arquiteta Nastache Soares. Resolvida na forma de demonstrar as ideias projetadas para  atender diversos estilos de vida, Juliana costuma mexer mais profundamente com os desejos dos clientes nas empreitadas que assume.
Seus traçados sugerem mais do que rotinas a serem preenchidas, mas cenário para aproveitar as coisas boas que acontecem no dia a dia.
Formada em 2000 em arquitetura e urbanismo pela Universidade Federal de Santa Catarina, hoje, destaca-se pelo profissionalismo, competência e autenticidade no mercado nacional.
Já ganhou também diversas premiações, uma forma de reconhecimento da qualidade e grandiosidade dos seus trabalhos voltados para projetos de interiores, arquitetônicos residenciais e comerciais.

		</strong>
	<br/>		
	<br/>	
	<h1>ÁREAS DE ATUAÇÃO</h1>
	<label for="name">
	<strong>
    - Obras de terraplenagem;
    - Instalação e manutenção elétrica;
    - Instalações de sistema de prevenção contra incêndio;
    - Construção de obras de arte especiais;
    - Outras obras de acabamento da construção;
    - Construção de rodovias e ferrovias;
    - Manutenção de redes de distribuição de energia elétrica;
    - Manutenção de estações e redes de telecomunicações;
    - Serviços de desenho técnico relacionados à arquitetura e engenharia;
    - Atividades Paisagísticas.
	</strong>
	</label>
</form>
</body>
</html>