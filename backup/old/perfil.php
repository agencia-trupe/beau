<link href="css/beau.css.css" rel="stylesheet" type="text/css">
<table width="500" height="375" border="0" cellpadding="0" cellspacing="0">
<td width="500" height="30">&nbsp;</td>
<tr>
  <td width="500" height="5" bgcolor="#535657"></td>
<tr>
  <td width="500" height="5" align="left" valign="top"></td>
<tr>
  <td width="500" height="300" align="left" valign="top"> <table width="160" height="160"border="0" align="left" cellpadding="0" cellspacing="0">
    <tr>
      <td><img src="site.imagens/foto.jpg" alt="" width="150" height="150" /></td>
    </tr>
  </table>
    <span class="menu_titulos">Nastashe Soares</span><br>
    <span class="text">Arquiteta e urbanista graduada pela Universidade de Brasília em 2011. Através do escritório BEAU Arquitetura e Design, o qual surgiu da necessidade de criar uma identidade para o seu trabalho, busca reproduzir e materializar os desejos e sonhos das pessoas em ambientes agradáveis e confortáveis. Originalidade, funcionalidade, linguagem contemporânea, bases neutras, cores em detalhes e principalmente respeito ao cliente caracterizam o trabalho da profissional e contribuem para a formação do conceito do escritório. </span></td>
</table>